import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Index from '@/view/index'
import GetQQ from "../view/application/getQQ";        //QQ信息获取
import Weather from "../view/application/weather";    //天气信息获取
import NowInHistory from "../view/application/nowInHistory";   //历史上的今天
import Rubbish from "../view/application/rubbish";    //垃圾分类

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: {
        show_header: true,      //顶部标题栏
        show_header2: false,    //顶部标题栏，带返回
        title_text: 'ruarua'
      }
    },
    {
      path: '/getQQ',
      name: 'GetQQ',
      component: GetQQ,
      meta: {
        show_header: false,      //顶部标题栏
        show_header2: true,      //顶部标题栏，带返回
        title_text: 'QQ头像昵称'
      }
    },
    {
      path: '/weather',
      name: 'Weather',
      component: Weather,
      meta: {
        show_header: false,      //顶部标题栏
        show_header2: true,      //顶部标题栏，带返回
        title_text: '天气预报'
      }
    },
    {
      path: '/nowInHistory',
      name: 'NowInHistory',
      component: NowInHistory,
      meta: {
        show_header: false,      //顶部标题栏
        show_header2: true,      //顶部标题栏，带返回
        title_text: '历史上的今天'
      }
    },
    {
      path: '/rubbish',
      name: 'Rubbish',
      component: Rubbish,
      meta: {
        show_header: false,      //顶部标题栏
        show_header2: true,      //顶部标题栏，带返回
        title_text: '你是什么垃圾'
      }
    },
    // {
    //   path: 'helloWorld',
    //   name: 'HelloWorld',
    //   component: HelloWorld
    // }
  ]
})
